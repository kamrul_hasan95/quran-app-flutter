// To parse required this JSON data, do
//
//     final surahNameList = surahNameListFromJson(jsonString);

import 'dart:convert';

SurahNameList surahNameListFromJson(String str) =>
    SurahNameList.fromJson(json.decode(str));

class SurahNameList {
  List<Chapter> chapters;

  SurahNameList({
    required this.chapters,
  });

  factory SurahNameList.fromJson(Map<String, dynamic> json) => SurahNameList(
        chapters: List<Chapter>.from(
            json["chapters"].map((x) => Chapter.fromJson(x))),
      );
}

class Chapter {
  String arName;
  String bnName;
  String bnSl;
  String chapterId;
  String enName;
  String id;
  String raw;
  String rukus;
  String start;
  String trName;
  Type type;
  String verseCount;
  int offline;
  int downloaded;

  Chapter({
    required this.arName,
    required this.bnName,
    required this.bnSl,
    required this.chapterId,
    required this.enName,
    required this.id,
    required this.raw,
    required this.rukus,
    required this.start,
    required this.trName,
    required this.type,
    required this.verseCount,
    required this.offline,
    required this.downloaded,
  });

  factory Chapter.fromJson(Map<String, dynamic> json) => Chapter(
        arName: json["ar_name"],
        bnName: json["bn_name"],
        bnSl: json["bn_sl"],
        chapterId: json["chapter_id"],
        enName: json["en_name"],
        id: json["id"],
        raw: json["raw"],
        rukus: json["rukus"],
        start: json["start"],
        trName: json["tr_name"],
        type: typeValues[json["type"]] ?? Type.makki,
        verseCount: json["verse_count"],
        offline: json["offline"],
        downloaded: json["downloaded"],
      );
}

enum Type { makki, madani }

final typeValues = {"মাক্কী": Type.makki, "মাদানী": Type.madani};
