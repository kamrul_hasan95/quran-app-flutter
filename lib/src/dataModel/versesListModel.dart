// To parse required this JSON data, do
//
//     final verse = verseFromJson(jsonString);

import 'dart:convert';

Verse verseFromJson(String str) => Verse.fromJson(json.decode(str));

class Verse {
  List<VerseElement> verses;

  Verse({
    required this.verses,
  });

  factory Verse.fromJson(Map<String, dynamic> json) => Verse(
        verses: List<VerseElement>.from(
            json["verses"].map((x) => VerseElement.fromJson(x))),
      );
}

class VerseElement {
  String ar;
  String bayaan;
  String bnName;
  String bnTafseer;
  String bnsl;
  String chapterId;
  String en;
  String id;
  String name;
  String transliteration;
  String verseId;

  VerseElement({
    required this.ar,
    required this.bayaan,
    required this.bnName,
    required this.bnTafseer,
    required this.bnsl,
    required this.chapterId,
    required this.en,
    required this.id,
    required this.name,
    required this.transliteration,
    required this.verseId,
  });

  factory VerseElement.fromJson(Map<String, dynamic> json) => VerseElement(
        ar: json["ar"],
        bayaan: json["bayaan"],
        bnName: json["bn_name"],
        bnTafseer: json["bn_tafseer"],
        bnsl: json["bnsl"],
        chapterId: json["chapter_id"],
        en: json["en"],
        id: json["id"],
        name: json["name"],
        transliteration: json["transliteration"],
        verseId: json["verse_id"],
      );
}
