import 'package:flutter/material.dart';
import 'package:flutter_quran_app/src/constants/constants.dart';

class HomeView extends StatefulWidget {
  @override
  _HomeViewState createState() => _HomeViewState();
}

class _HomeViewState extends State<HomeView> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("Xyz"),
      ),
      body: Container(
        child: Center(
          child: TextButton(
            onPressed: () {
              Navigator.pushNamed(context, SCREEN_NAME_SURAH_LIST_FOR_READING);
            },
            child: Text("Surah List"),
          ),
        ),
      ),
    );
  }
}
