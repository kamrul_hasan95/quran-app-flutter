import 'dart:async';

import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:flutter_quran_app/src/appModule/reading/surahDetailsView/presenter/surahDetailsPresenter.dart';
import 'package:flutter_quran_app/src/dataModel/versesListModel.dart';
import 'package:shared_preferences/shared_preferences.dart';

class SurahVersesListView extends StatefulWidget {
  final String surahName;
  final String chapterId;

  const SurahVersesListView({
    Key? key,
    required this.surahName,
    required this.chapterId,
  }) : super(key: key);

  @override
  State<SurahVersesListView> createState() => _SurahVersesListViewState();
}

class _SurahVersesListViewState extends State<SurahVersesListView>
    implements VersesScreenContact {
  List<VerseElement>? verses;

  late VersesPresenter presenter;

  late ScrollController _controller;

  Future<SharedPreferences> sharedPreferences = SharedPreferences.getInstance();
  late SharedPreferences prefs;

  double _scrollPosition = 0;

  @override
  void initState() {
    _controller = ScrollController();
    super.initState();
    sharedPreferences.then((value) {
      prefs = value;
      _scrollPosition =
          prefs.getDouble('SURAH_READ_AREA_${widget.chapterId}') ?? 0.0;
    });

    _controller.addListener(_scrollListener);

    presenter = VersesPresenter(this);

    presenter.loadVerses(widget.chapterId);
  }

  _scrollListener() {
    _scrollPosition = _controller.offset;
    if (kDebugMode) {
      print(_scrollPosition.toString());
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(widget.surahName),
      ),
      body: Container(
        child: verses == null
            ? const Center(
                child: CircularProgressIndicator(),
              )
            : ListView.builder(
                controller: _controller,
                itemCount: verses!.length,
                itemBuilder: (context, index) {
                  return GestureDetector(
                    child: VerseItemView(verses![index]),
                    onTap: () {},
                  );
                },
              ),
      ),
    );
  }

  @override
  void onVerses(List<VerseElement> verses) async {
    setState(() {
      this.verses = verses;
    });

    Timer(const Duration(milliseconds: 1000), () {
      _controller.animateTo(_scrollPosition,
          curve: Curves.linear, duration: const Duration(milliseconds: 500));
    });
  }

  @override
  void dispose() {
    prefs.setDouble('SURAH_READ_AREA_${widget.chapterId}', _scrollPosition);
    super.dispose();
  }
}

class VerseItemView extends StatelessWidget {
  final VerseElement verseElement;

  const VerseItemView(this.verseElement, {super.key});

  @override
  Widget build(BuildContext context) {
    return Card(
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.circular(8.0),
      ),
      margin: const EdgeInsets.all(8.0),
      child: Container(
        padding: const EdgeInsets.all(
          8.0,
        ),
        decoration: BoxDecoration(
          borderRadius: BorderRadius.circular(
            8.0,
          ),
          border: Border.all(
            color: Colors.blue,
            width: 2.0,
          ),
        ),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.center,
          children: <Widget>[
            Padding(
              padding: const EdgeInsets.all(4.0),
              child: Text(
                verseElement.bnsl,
                style: const TextStyle(
                  fontSize: 17.0,
                ),
              ),
            ),
            Padding(
              padding: const EdgeInsets.all(4.0),
              child: Text(
                verseElement.ar,
                style: const TextStyle(
                  fontSize: 17.0,
                ),
              ),
            ),
            Padding(
              padding: const EdgeInsets.all(4.0),
              child: Text(
                verseElement.bayaan,
                style: const TextStyle(
                  fontSize: 17.0,
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
