import 'package:flutter/services.dart' show rootBundle;
import 'package:flutter/services.dart';
import 'package:flutter_quran_app/src/dataModel/versesListModel.dart';

abstract class VersesScreenContact {
  void onVerses(List<VerseElement> verses);
}

class VersesPresenter {
  VersesScreenContact _view;

  VersesPresenter(this._view);

  Future<String> _loadCrosswordAsset() async {
    return await rootBundle.loadString('assets/raw/quran_all_verses.json');
  }

  void loadVerses(String chapterId) async {
    Verse verse;
    _loadCrosswordAsset().then(
      (String jsonString) async {
        verse = verseFromJson(jsonString);

        await getVerses(verse, chapterId);
      },
    );
  }

  Future<void> getVerses(Verse verse, String chapterId) async {
    List<VerseElement> verses = [];
    for (int i = 0; i < verse.verses.length; i++) {
      if (chapterId == verse.verses[i].chapterId) {
        verses.add(verse.verses[i]);
      }
    }

    _view.onVerses(verses);
  }
}
