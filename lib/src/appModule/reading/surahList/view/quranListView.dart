import 'package:flutter/material.dart';
import 'package:flutter_quran_app/src/appModule/reading/surahDetailsView/view/surahDetailsView.dart';
import 'package:flutter_quran_app/src/appModule/reading/surahList/presenter/surahListPresenter.dart';
import 'package:flutter_quran_app/src/dataModel/surahNameListModel.dart';

class QuranLIstView extends StatefulWidget {
  const QuranLIstView({Key? key}) : super(key: key);

  @override
  State<QuranLIstView> createState() => _QuranLIstViewState();
}

class _QuranLIstViewState extends State<QuranLIstView>
    implements SurahListScreenContact {
  SurahNameList? surahNameList;

  late SurahListPresenter presenter;
  @override
  void initState() {
    super.initState();

    presenter = SurahListPresenter(this);
    presenter.loadSurahList();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text("Surah List"),
      ),
      body: Container(
        child: surahNameList == null
            ? const Center(
                child: CircularProgressIndicator(),
              )
            : ListView.builder(
                itemCount: surahNameList!.chapters.length,
                itemBuilder: (context, index) {
                  return GestureDetector(
                    child: SurahItemView(
                      surahNameList!.chapters[index],
                    ),
                    onTap: () {
                      Navigator.push(
                        context,
                        MaterialPageRoute(
                          builder: (context) => SurahVersesListView(
                            surahName: surahNameList!.chapters[index].bnName,
                            chapterId: surahNameList!.chapters[index].chapterId,
                          ),
                        ),
                      );
                    },
                  );
                },
              ),
      ),
    );
  }

  @override
  void onSurahListFetched(SurahNameList surahNameList) {
    setState(() {
      this.surahNameList = surahNameList;
    });
  }
}

class SurahItemView extends StatelessWidget {
  final Chapter chapter;

  const SurahItemView(this.chapter, {super.key});

  @override
  Widget build(BuildContext context) {
    return Card(
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.circular(8.0),
      ),
      elevation: 10.0,
      margin: const EdgeInsets.all(8.0),
      child: Container(
        padding: const EdgeInsets.all(
          8.0,
        ),
        decoration: BoxDecoration(
          borderRadius: BorderRadius.circular(
            8.0,
          ),
          border: Border.all(
            color: Colors.blue,
            width: 2.0,
          ),
        ),
        child: Column(
          children: <Widget>[
            Padding(
              padding: const EdgeInsets.all(4.0),
              child: Text(
                chapter.id,
                style: const TextStyle(
                  fontSize: 22.0,
                  fontWeight: FontWeight.w600,
                ),
              ),
            ),
            Padding(
              padding: const EdgeInsets.all(4.0),
              child: Text(
                chapter.bnName,
                style: const TextStyle(
                  fontSize: 22.0,
                  fontWeight: FontWeight.w600,
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
