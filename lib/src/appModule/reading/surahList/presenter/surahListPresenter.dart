import 'package:flutter/services.dart' show rootBundle;
import 'package:flutter_quran_app/src/dataModel/surahNameListModel.dart';

abstract class SurahListScreenContact {
  void onSurahListFetched(SurahNameList surahNameList);
}

class SurahListPresenter {
  SurahListScreenContact _view;

  SurahListPresenter(this._view);

  Future<String> _loadCrosswordAsset() async {
    return await rootBundle.loadString('assets/raw/quran_all_surah_list.json');
  }

  void loadSurahList() async {
    SurahNameList surahNameList;
    _loadCrosswordAsset().then(
      (String jsonString) {
        surahNameList = surahNameListFromJson(jsonString);

        _view.onSurahListFetched(surahNameList);
      },
    );
  }
}
