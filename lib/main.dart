import 'package:flutter/material.dart';
import 'package:flutter/services.dart';

import 'src/appModule/reading/surahList/view/quranListView.dart';

void main() {
  WidgetsFlutterBinding.ensureInitialized();
  SystemChrome.setPreferredOrientations([DeviceOrientation.portraitUp]).then(
    (_) {
      runApp(
        MaterialApp(
          initialRoute: '/',
          routes: {
            '/': (context) => QuranLIstView(),
//            SCREEN_NAME_SURAH_LIST_FOR_READING: (context) => QuranLIstView(),
          },
        ),
      );
    },
  );
}
